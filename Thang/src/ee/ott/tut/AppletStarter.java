package ee.ott.tut;

public class AppletStarter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 Main applet = new Main();
         applet.init();
         applet.start();
 
         javax.swing.JFrame window = new javax.swing.JFrame("Karnaugh Map Logic Minimizer");
         
         window.setContentPane(applet);
         window.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
         window.setSize(860, 640);
         window.pack();
         window.setVisible(true);

	}

}
