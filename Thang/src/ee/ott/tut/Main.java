package ee.ott.tut;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;

import lib.Implicant;
import lib.Quine;

public class Main extends JApplet implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int width, height;
	int x, y;
	Graphics g;

	JPanel mainPanel;
	JButton startButton;
	JButton clearButton;
	JLabel inputOnesLabel;
	JTextField inputOnesField;
	JLabel inputAnyLabel;
	JTextField inputAnyField;
	JLabel inputVariablesLabel;
	JSpinner inputVariablesField;

	GridBagLayout mainPanelLayout;
	GridBagConstraints mainPanelConstraints;

	JPanel kPanel;
	JButton kGenerateButton;
	JButton kClearButton;
	JButton kSolveButton;
	JLabel kVariablesLabel;
	JSpinner kVariablesField;
	Set<Implicant> contours;

	JPanel langPanel;
	GridLayout langPanelLayout;
	JButton enButton;
	JButton etButton;
	JButton ruButton;

	JPanel mapPanel1;
	JPanel mapPanel2;

	GridBagLayout kPanelLayout;
	GridBagConstraints kPanelConstraints;

	JPanel wrapper;
	KarnaughMap map;
	Random rnd;

	JPanel consolePanel;
	GridLayout consolePanelLayout;
	JTextArea console;
	JScrollPane consoleScroller;

	StringBuilder stringBuilder;
	String function;



	private static String inVarLabel = "Enter the number of variables";
	private static String startButtonText = "Start";
	private static String generateButtonText = "Generate map";
	private static String clearButtonText = "Clear";
	private static String solveButtonText = "Solve";
	private static String evaluteOnesText = "Enter terms that evaluate to 1, separated by spaces";
	private static String evaluteAnysText = "Enter terms that evaluate to -, separated by spaces";

	public static void main() {
		
	}
	public void init() {
		width = getSize().width;
		height = getSize().height;

		x = width/2;
		y = height/2;

		rnd = new Random();

		setSize(860, 640);
		this.setLayout(new GridLayout(2,2));

		mainPanel = new JPanel();
		mainPanelLayout = new GridBagLayout();

		mainPanel.setLayout(mainPanelLayout);
		mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.gridwidth = 1;

		startButton = new JButton(startButtonText, null);
		clearButton = new JButton(clearButtonText, null);
		startButton.addActionListener(this);
		clearButton.addActionListener(this);

		inputVariablesLabel = new JLabel(inVarLabel);
		inputVariablesField = new JSpinner(new SpinnerNumberModel(2,2,26,1));

		inputOnesLabel = new JLabel(evaluteOnesText);
		inputOnesField = new JTextField(15);
		inputAnyLabel = new JLabel(evaluteAnysText);
		inputAnyField = new JTextField(15);

		startButton.setVisible(true);
		clearButton.setVisible(true);

		inputOnesField.setVisible(true);
		inputOnesLabel.setVisible(true);

		inputVariablesField.setVisible(true);
		inputVariablesLabel.setVisible(true);

		inputAnyField.setVisible(true);
		inputAnyLabel.setVisible(true);


		enButton = new JButton("EN", null);
		ruButton = new JButton("RU", null);
		etButton = new JButton("ET", null);
		enButton.addActionListener(this);
		ruButton.addActionListener(this);
		etButton.addActionListener(this);
		langPanel = new JPanel();
		langPanelLayout = new GridLayout(1,3);
		enButton.setVisible(true);
		ruButton.setVisible(true);
		etButton.setVisible(true);
		langPanel.add(enButton);
		langPanel.add(ruButton);
		langPanel.add(etButton);

		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(langPanel);		

		mainPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(inputVariablesLabel, mainPanelConstraints);
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 1;
		mainPanelConstraints.weightx = 1.0;
		mainPanelConstraints.weighty = 1.0;
		mainPanel.add(inputVariablesField, mainPanelConstraints);
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 2;
		mainPanelConstraints.weightx = 0.0;
		mainPanelConstraints.weighty = 0.0;
		mainPanel.add(inputOnesLabel, mainPanelConstraints);
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 2;
		mainPanelConstraints.weightx = 1.0;
		mainPanelConstraints.weighty = 1.0;
		mainPanel.add(inputOnesField, mainPanelConstraints);
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 3;
		mainPanelConstraints.weightx = 0.0;
		mainPanelConstraints.weighty = 0.0;
		mainPanel.add(inputAnyLabel, mainPanelConstraints);
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 3;
		mainPanelConstraints.weightx = 1.0;
		mainPanelConstraints.weighty = 1.0;
		mainPanel.add(inputAnyField, mainPanelConstraints);		
		mainPanelConstraints.fill = GridBagConstraints.REMAINDER;
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(startButton, mainPanelConstraints);		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(clearButton, mainPanelConstraints);
		mainPanel.setVisible(true);

		kPanel = new JPanel();		
		kPanelLayout = new GridBagLayout();
		kPanel.setLayout(kPanelLayout);
		kPanelConstraints = new GridBagConstraints();

		kGenerateButton = new JButton(generateButtonText, null);
		kClearButton = new JButton(clearButtonText, null);
		kSolveButton = new JButton(solveButtonText, null);
		kGenerateButton.addActionListener(this);
		kClearButton.addActionListener(this);
		kSolveButton.addActionListener(this);		

		kVariablesLabel = new JLabel(inVarLabel);
		kVariablesField = new JSpinner(new SpinnerNumberModel(2,2,5,1));		

		kPanelConstraints.gridwidth = 1;

		kGenerateButton.setVisible(true);
		kClearButton.setVisible(true);
		kSolveButton.setVisible(true);
		kSolveButton.setEnabled(false);
		kVariablesLabel.setVisible(true);
		kVariablesField.setVisible(true);

		kPanelConstraints.gridx = 0;
		kPanelConstraints.gridy = 0;
		kPanel.add(kVariablesLabel, kPanelConstraints);
		kPanelConstraints.gridx = 1;
		kPanelConstraints.gridy = 0;
		kPanel.add(kVariablesField, kPanelConstraints);
		kPanelConstraints.gridx = 0;
		kPanelConstraints.gridy = 1;
		kPanel.add(kGenerateButton, kPanelConstraints);
		kPanelConstraints.gridx = 1;
		kPanelConstraints.gridy = 1;
		kPanel.add(kClearButton, kPanelConstraints);
		kPanelConstraints.gridx = 0;
		kPanelConstraints.gridy = 2;
		kPanel.add(kSolveButton, kPanelConstraints);
		kPanel.setVisible(true);

		wrapper = new JPanel();
		wrapper.setLayout(new FlowLayout());

		consolePanel = new JPanel();
		consolePanelLayout = new GridLayout();

		consolePanel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		consolePanel.setLayout(consolePanelLayout);

		console = new JTextArea("");
		consoleScroller = new JScrollPane(console);

		consolePanel.add(consoleScroller);

		console.setVisible(true);
		consolePanel.setVisible(true);


		this.add(kPanel);
		this.add(mainPanel);
		this.add(wrapper);
		this.add(consolePanel);
		this.setVisible(true);

		repaint();
		resize(getWidth(), getHeight());
	}

	public void start(){
		super.start();		
		this.validate();
		this.repaint();
		this.resize(this.getWidth(), this.getHeight());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int numVariables = 0;
		String status;
		ArrayList<JLabel> horizontalLabelList;
		ArrayList<JLabel> verticalLabelList;
		Iterator<JLabel> horLabelIter;
		Iterator<JLabel> verLabelIter;
		function = inputVariablesField.getValue() + " m " + inputOnesField.getText() + " d " + inputAnyField.getText() + " a ";

		if(e.getSource() == startButton) {
			Quine quine = new Quine();
			status = quine.parseInput(function);
			if(status.equals("Success")) {
				contours = quine.run(function);
				if(contours != null) {
					console.append(quine.printExpression(contours));
				}
			}
			else {
				console.append("\n"+status);
			}
		}
		else if(e.getSource() == clearButton) {
			inputOnesField.setText("");
			inputAnyField.setText("");
			console.setText("");
		}
		else if(e.getSource() == kGenerateButton) {
			contours = null;
			wrapper.removeAll();
			wrapper.repaint();
			numVariables = (Integer) kVariablesField.getValue();
			map = new KarnaughMap((Integer) kVariablesField.getValue());
			mapPanel1 = new JPanel();
			mapPanel2 = new JPanel();
			horizontalLabelList = map.getHorizontalLabels();
			verticalLabelList = map.getVerticalLabels();
			horLabelIter = horizontalLabelList.iterator();
			verLabelIter = verticalLabelList.iterator();

			KarnaughButton tmpButton = null;
			if(numVariables == 2) {
				mapPanel1.add(new JLabel("x1\\x2"));
				for(int i = 0; i < 2; i++) {
					mapPanel1.add(horLabelIter.next());
				}
			}
			else if(numVariables == 3) {
				mapPanel1.add(new JLabel("x1\\x2x3"));
				for(int i = 0; i < 4; i++) {
					mapPanel1.add(horLabelIter.next());
				}
			}
			else if(numVariables == 4) {
				mapPanel1.add(new JLabel("x1x2\\x3x4"));
				for(int i = 0; i < 4; i++) {
					mapPanel1.add(horLabelIter.next());
				}
			}
			else if(numVariables == 5) {
				mapPanel1.add(new JLabel("0|x2x3\\x4x5"));
				for(int i = 0; i < 4; i++) {
					mapPanel1.add(horLabelIter.next());
				}
				mapPanel2.add(new JLabel("1|x2x3\\x4x5"));
				for(int i = 0; i < 4; i++) {
					mapPanel2.add(horLabelIter.next());
				}
			}
			int counter = 0;
			for(int ordinal : map.getOrder()) {
				tmpButton = map.getButtons().get(ordinal);

				tmpButton.addActionListener(this);
				for(MouseMotionListener listener : tmpButton.getMouseMotionListeners()) {
					tmpButton.removeMouseMotionListener(listener);
				}
				tmpButton.setRolloverEnabled(false);
				if(ordinal < 16) {
					if(numVariables == 2) {
						if(counter % 2 == 0) {
							mapPanel1.add(verLabelIter.next());							
						}
					}
					if(numVariables != 2) {
						if(counter % 4 == 0) {
							mapPanel1.add(verLabelIter.next());							
						}
					}
					mapPanel1.add(tmpButton);
				}
				else {
					if(counter % 4 == 0) {
						mapPanel2.add(verLabelIter.next());						
					}
					mapPanel2.add(tmpButton);
				}
				counter++;
				tmpButton.setVisible(true);
				invalidate();
				repaint();
				validate();
			}

			GridLayout mapPanelLayout = null;
			if(numVariables == 2) {
				mapPanelLayout = new GridLayout(3,3);
			}
			else if(numVariables == 3) {
				mapPanelLayout = new GridLayout(3,5);
			}
			else if(numVariables == 4) {
				mapPanelLayout = new GridLayout(5,5);
			}
			else if(numVariables == 5) {
				mapPanelLayout = new GridLayout(5,5);
			}
			mapPanel1.setLayout(mapPanelLayout);
			mapPanel2.setLayout(mapPanelLayout);

			wrapper.add(mapPanel1);
			wrapper.add(mapPanel2);

			kSolveButton.setEnabled(true);
			console.setText("");

			invalidate();			
			repaint();
			validate();

		}
		else if(e.getSource() instanceof KarnaughButton) {
			KarnaughButton tempButton = (KarnaughButton) e.getSource();
			tempButton.click();
		}
		else if(e.getSource() == kSolveButton) {
			kSolveButton.setEnabled(false);
			Quine quine = new Quine();
			status = quine.parseInput(function);
			if(status.equals("Success")) {
				contours = quine.run(map.getFunction());
				if(contours != null) {
					console.append(quine.printExpression(contours));
				}
			}
			else {
				console.append("\n"+status);
			}
			drawMap();
			for(KarnaughButton button : map.getButtons()) {
				button.removeActionListener(this);
				for(MouseListener listener : button.getMouseListeners()) {
					button.removeMouseListener(listener);
				}
			}
		}
		else if(e.getSource() == kClearButton) {
			for(Component comp : wrapper.getComponents()) {
				comp.setVisible(false);
			}
			wrapper.removeAll();
			kSolveButton.setEnabled(false);
			console.setText("");
			invalidate();
			validate();
		}
		else if(e.getSource() == enButton) {
			changeLanguage(enButton);
		}
		else if(e.getSource() == ruButton) {
			changeLanguage(ruButton);
		}
		else if(e.getSource() == etButton) {
			changeLanguage(etButton);
		}
		this.resize(this.getWidth(), this.getHeight());
	}

	public static ArrayList<Set<Integer>> printTruthTable(Set<Implicant> contours, int n) {
		ArrayList<Set<Integer>> contoursAsInt = new ArrayList<Set<Integer>>();
		for(int i = 0; i < (int) Math.pow(2,n); i++) {
			contoursAsInt.add(new HashSet<Integer>());
		}
		int rows = (int) Math.pow(2,n);
		ArrayList<String> strings = new ArrayList<String>();
		String tmpString;
		int contourCounter = 0;
		for(Implicant implicant : contours) {
			tmpString = "";
			for (int i=0; i<rows; i++) {
				for (int j=n-1; j>=0; j--) {
					tmpString = tmpString.concat(String.valueOf((i/(int) Math.pow(2, j))%2));
					System.out.print((i/(int) Math.pow(2, j))%2);
				}
				strings.add(tmpString);			
				tmpString = "";
				System.out.println();
			}
			String regex = makeImplicantRegex(implicant.toString());
			System.out.println("Implicant: " + implicant.toString());
			for(String string: strings) {
				if(string.matches(regex)) {
					contoursAsInt.get(contourCounter).add(Integer.parseInt(string, 2));
					System.out.println("Matched: " + string + " Dec: " + Integer.parseInt(string, 2));
				}
			}
			contourCounter++;
		}
		return contoursAsInt;
	}

	private static String makeImplicantRegex(String implicant) {
		String regex = "";
		regex = implicant.replaceAll("-", "\\\\d");
		return regex;
	}

	public static int countOccurrences(String haystack, char needle)
	{
		int count = 0;
		for (int i=0; i < haystack.length(); i++)
		{
			if (haystack.charAt(i) == needle)
			{
				count++;
			}
		}
		return count;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		drawMap();
	}

	protected void drawMap() {
		int numOfVars = (Integer) kVariablesField.getValue();
		if(contours != null) {
			int borderThickness = contours.size()-1;
			KarnaughButton tmpButton;
			Color color = new Color(50, 100, 150);
			for(Set<Integer> integers : printTruthTable(contours, numOfVars)) {
				for(Integer integer : integers) {
					tmpButton = map.getButtons().get(integer);
					g = tmpButton.getParent().getGraphics();
					g.setColor(color);
					for(int i = 0; i < 1; i++) {
						g.drawOval(tmpButton.getX()+i*2 + borderThickness*2, tmpButton.getY()+i*2 + borderThickness*2,
								tmpButton.getSize().width-i*2 - borderThickness*4, tmpButton.getHeight()-i*2 - borderThickness*4);
					}
				}
				color = new Color((color.getRed()+60)%255, (color.getGreen()+60)%255, (color.getBlue()+60)%255);
				borderThickness-=1;
			}
		}
		resize(getWidth(), getHeight());
		validate();
	}

	protected void changeLanguage(JButton langButton) {
		if(langButton == enButton) {			
			inputVariablesLabel.setText("Enter the number of variables");
			kVariablesLabel.setText("Enter the number of variables");
			startButton.setText("Solve");
			kGenerateButton.setText("Generate map");
			kClearButton.setText("Clear");
			clearButton.setText("Clear");
			kSolveButton.setText("Solve");
			inputOnesLabel.setText("Enter terms that evaluate to 1, separated by spaces");
			inputAnyLabel.setText("Enter terms that evaluate to -, separated by spaces");
			
		}
		else if(langButton == ruButton) {
			inputVariablesLabel.setText("Выберите число параметры");
			kVariablesLabel.setText("Выберите число параметры");
			startButton.setText("решaй");
			kGenerateButton.setText("рисовай карта");
			kClearButton.setText("начисто");
			clearButton.setText("начисто");
			kSolveButton.setText("решай");
			inputOnesLabel.setText("условия, которые оценивают в 1, разделенных пробелами");
			inputAnyLabel.setText("условия, которые оценивают в -, разделенных пробелами");
		}
		else if(langButton == etButton) {
			inputVariablesLabel.setText("Sisesta muutujate arv");
			kVariablesLabel.setText("Sisesta muutujate arv");
			startButton.setText("Lahenda");
			kGenerateButton.setText("Genereeri kaart");
			kClearButton.setText("Puhasta");
			clearButton.setText("Puhasta");
			kSolveButton.setText("Lahenda");
			inputOnesLabel.setText("Sisesta 1'de piirkond, eraldades väärtused tühikuga");
			inputAnyLabel.setText("Sisesta -'de piirkond, eraldades väärtused tühikuga");
			
		}
		
	}
}
