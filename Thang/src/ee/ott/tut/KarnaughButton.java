package ee.ott.tut;

import java.awt.Color;
import java.awt.HeadlessException;

import javax.swing.JButton;

public class KarnaughButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LogValue logValue;
	private int decValue;
	
	public enum LogValue {
		ONE, ZERO, ANY
	}
	
	public KarnaughButton(LogValue value, int decValue) throws HeadlessException {
		super("0");
		this.logValue = value;
		this.decValue = decValue;
		this.setBackground(Color.WHITE);
	}

	public int getDecValue() {
		return decValue;
	}

	public void setDecValue(int decValue) {
		this.decValue = decValue;
	}

	public LogValue getLogValue() {
		return logValue;
	}

	public void setLogValue(LogValue value) {
		this.logValue = value;
	}
	
	public void click() {
		if(logValue == LogValue.ZERO) {
			logValue = LogValue.ONE;
			setText("1");
		}
		else if(logValue == LogValue.ONE) {
			logValue = LogValue.ANY;
			setText("-");
		}
		else if(logValue == LogValue.ANY){
			logValue = LogValue.ZERO;
			setText("0");
		}
		
	}
}
