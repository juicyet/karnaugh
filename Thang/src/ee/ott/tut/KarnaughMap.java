package ee.ott.tut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;

import ee.ott.tut.KarnaughButton.LogValue;

public class KarnaughMap {
	
	Map<Integer,KarnaughButton> map;
	private ArrayList<KarnaughButton> buttons;
	private ArrayList<JLabel> horizontalLabels;
	private ArrayList<JLabel> verticalLabels;
	private int numVariables;
	private String function;
	private int[] order;
	
	public KarnaughMap(int numVariables) {
		this.numVariables = numVariables;
		this.map = new HashMap<Integer, KarnaughButton>();
		this.buttons = new ArrayList<KarnaughButton>();
		this.order = new int[(int)Math.pow(2, numVariables)];
		this.horizontalLabels = new ArrayList<JLabel>();
		this.verticalLabels = new ArrayList<JLabel>();
		init(numVariables);
		
	}
	
	private void init(int numVariables) {
		for(int i = 0; i < Math.pow(2, numVariables); i++) {
			buttons.add(new KarnaughButton(LogValue.ZERO, i));
			//map.put(i, new KarnaughButton(LogValue.ZERO, i));
		}
		
		
		if(numVariables == 2) {
			order[0] = 0;
			order[1] = 1;
			order[2] = 2;
			order[3] = 3;
			horizontalLabels.add(new JLabel("0", JLabel.CENTER));
			horizontalLabels.add(new JLabel("1", JLabel.CENTER));
			verticalLabels.add(new JLabel("0", JLabel.CENTER));
			verticalLabels.add(new JLabel("1", JLabel.CENTER));
			
		}
		else if(numVariables == 3) {
			order[0] = 0;
			order[1] = 1;
			order[2] = 3;
			order[3] = 2;
			order[4] = 4;
			order[5] = 5;
			order[6] = 7;
			order[7] = 6;
			horizontalLabels.add(new JLabel("00", JLabel.CENTER));
			horizontalLabels.add(new JLabel("01", JLabel.CENTER));
			horizontalLabels.add(new JLabel("11", JLabel.CENTER));
			horizontalLabels.add(new JLabel("10", JLabel.CENTER));
			verticalLabels.add(new JLabel("0", JLabel.CENTER));
			verticalLabels.add(new JLabel("1", JLabel.CENTER));
		}
		else if(numVariables == 4) {
			order[0] = 0;
			order[1] = 1;
			order[2] = 3;
			order[3] = 2;
			order[4] = 4;
			order[5] = 5;
			order[6] = 7;
			order[7] = 6;
			order[8] = 12;
			order[9] = 13;
			order[10] = 15;
			order[11] = 14;
			order[12] = 8;
			order[13] = 9;
			order[14] = 11;
			order[15] = 10;
			horizontalLabels.add(new JLabel("00", JLabel.CENTER));
			horizontalLabels.add(new JLabel("01", JLabel.CENTER));
			horizontalLabels.add(new JLabel("11", JLabel.CENTER));
			horizontalLabels.add(new JLabel("10", JLabel.CENTER));
			verticalLabels.add(new JLabel("00", JLabel.CENTER));
			verticalLabels.add(new JLabel("01", JLabel.CENTER));
			verticalLabels.add(new JLabel("11", JLabel.CENTER));
			verticalLabels.add(new JLabel("10", JLabel.CENTER));
		}
		else if(numVariables == 5) {
			order[0] = 0;
			order[1] = 1;
			order[2] = 3;
			order[3] = 2;
			order[4] = 4;
			order[5] = 5;
			order[6] = 7;
			order[7] = 6;
			order[8] = 12;
			order[9] = 13;
			order[10] = 15;
			order[11] = 14;
			order[12] = 8;
			order[13] = 9;
			order[14] = 11;
			order[15] = 10;
			order[16] = 16;
			order[17] = 17;
			order[18] = 19;
			order[19] = 18;
			order[20] = 20;
			order[21] = 21;
			order[22] = 23;
			order[23] = 22;
			order[24] = 28;
			order[25] = 29;
			order[26] = 31;
			order[27] = 30;
			order[28] = 24;
			order[29] = 25;
			order[30] = 27;
			order[31] = 26;
			horizontalLabels.add(new JLabel("00", JLabel.CENTER));
			horizontalLabels.add(new JLabel("01", JLabel.CENTER));
			horizontalLabels.add(new JLabel("11", JLabel.CENTER));
			horizontalLabels.add(new JLabel("10", JLabel.CENTER));
			horizontalLabels.add(new JLabel("00", JLabel.CENTER));
			horizontalLabels.add(new JLabel("01", JLabel.CENTER));
			horizontalLabels.add(new JLabel("11", JLabel.CENTER));
			horizontalLabels.add(new JLabel("10", JLabel.CENTER));
			verticalLabels.add(new JLabel("00", JLabel.CENTER));
			verticalLabels.add(new JLabel("01", JLabel.CENTER));
			verticalLabels.add(new JLabel("11", JLabel.CENTER));
			verticalLabels.add(new JLabel("10", JLabel.CENTER));
			verticalLabels.add(new JLabel("00", JLabel.CENTER));
			verticalLabels.add(new JLabel("01", JLabel.CENTER));
			verticalLabels.add(new JLabel("11", JLabel.CENTER));
			verticalLabels.add(new JLabel("10", JLabel.CENTER));
		}
		
	}

	public ArrayList<JLabel> getHorizontalLabels() {
		return horizontalLabels;
	}

	public ArrayList<JLabel> getVerticalLabels() {
		return verticalLabels;
	}

	public Map<Integer, KarnaughButton> getMap() {
		return map;
	}

	public int getNumVariables() {
		return numVariables;
	}

	public String getFunction() {
		boolean isAny = false;
		function = numVariables + " m";
		
		for(KarnaughButton button: buttons) {
			if(button.getLogValue() == LogValue.ONE) {
				function = function.concat(" " + button.getDecValue());
			}
			if(button.getLogValue() == LogValue.ANY) {
				isAny = true;
			}
		}
		if(isAny) {
			function = function.concat(" d");
			for(KarnaughButton button: buttons) {
				if(button.getLogValue() == LogValue.ANY) {
					function = function.concat(" " + button.getDecValue());
				}
			}
		}
		
		
		return function;
	}

	public ArrayList<KarnaughButton> getButtons() {
		return buttons;
	}

	public int[] getOrder() {
		return order;
	}
	
	
	
	
	
}
