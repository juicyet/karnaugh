package lib;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ee.ott.tut.Main;

public class Quine
{
	static int numVars;
	static boolean min;
	static List<Integer> terms;
	static List<Integer> dontCares;

	static String[][] contours;
	static String status;

	public Set<Implicant> run(String input)
	{
		terms = new LinkedList<Integer>();
		dontCares = new LinkedList<Integer>();
		status = parseInput(input);
		if(status.equals("Success")) {

			List<Integer> termsPlusDontCares = new LinkedList<Integer>();
			termsPlusDontCares.addAll(terms);
			termsPlusDontCares.addAll(dontCares);
			ImplicationTable t = new ImplicationTable(numVars, termsPlusDontCares);
			Set<Implicant> primes = t.getPrimeImplicants();

			for (Implicant imp : primes) {
				System.out.println("prime: "+imp);
			}

			PrimeImplicantChart chart = new PrimeImplicantChart(terms, primes);
			Set<Implicant> cover = chart.minimumCover();

			for (Implicant imp : cover) {
				System.out.println("cover: "+imp);
			}

			printExpression(cover);
			return cover;
		}

		return null;
	}



	public String printExpression(Set<Implicant> imps)
	{		
		String output = "";

		for (Implicant imp : imps) {
			output = output.concat("\nInterval: " + imp);
			System.out.println("cover: "+imp);
		}
		if(imps.size() == 0) {
			output = output.concat("\nf: 0\n");
			System.out.println("f: 0");
		}
		else {
			output = output.concat("\nf: ");		
			System.out.println("f: ");
		}
		boolean first = true;
		for (Implicant imp : imps) {
			if (!first) {
				if (min)
					System.out.print("+");
				output = output.concat("+");
			}
			first = false;

			System.out.print("(");
			output = output.concat("(");
			LinkedList<Bit> revImp = new LinkedList<Bit>();
			for (Bit b : imp.bits()) {
				revImp.addFirst(b);
			}
			int v = 1;
			boolean firstVar = true;
			for (Bit b : revImp) {
				String op = "";
				if ((!min) && (!firstVar)) {
					op = "+";
				}
				if (b instanceof BitOne) {
					output = output.concat(op+"x"+v);
					System.out.print(op+"x"+v);
				}
				if (b instanceof BitZero) {
					System.out.print(op+"~"+"x"+v);
					output = output.concat(op+"~"+"x"+v);
				}
				if (!(b instanceof BitBoth)) {
					firstVar = false;
				}

				v++;
			}
			if(revImp.size() == Main.countOccurrences(revImp.toString(), '-')) {
				System.out.print("1");
				output = output.concat("1");
			}
			System.out.print(")");
			output = output.concat(")");
		}
		output = output.concat("\n");
		System.out.println("");

		return output;
	}

	public String parseInput(String input)
	{
		terms = new LinkedList<Integer>();
		dontCares = new LinkedList<Integer>();
		Reader r = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()))));
		StreamTokenizer st = new StreamTokenizer(r);

		int ttype;
		try {
			ttype = st.nextToken();
			if (ttype == StreamTokenizer.TT_NUMBER) {
				numVars = (int)st.nval;
				if ((numVars >= 26) || (numVars < 0)) {
					System.err.println("Q-M 370 can only handle up to 26 varaibles");
					return "Q-M 370 can only handle up to 26 varaibles";
				}
			}
			else {
				System.err.println("Function spec must begin with the number of variables "+ ttype + " "+st.sval);
				return "Function spec must begin with the number of variables "+ ttype + " "+st.sval;
			}

			ttype = st.nextToken();
			if (ttype == StreamTokenizer.TT_WORD) {
				if (st.sval.equals("m"))
					min = true;
				else if (st.sval.equals("M"))
					min = false;
				else {
					System.err.println("Function spec must have \"m\" or \"M\" for min or max terms, not "+st.sval);
					return "Function spec must have \"m\" or \"M\" for min or max terms, not "+st.sval;
				}
			}
			else {
				System.err.println("Function spec must have \"m\" or \"M\" for min or max terms");
				return "Internal error";
			}

			// You should never write impenetrable bit twiddling code like the
			// following line.
			int mask = (((1 << 31) >> (numVars - 1))) >>> (32 - numVars);
			int maxTerm = 1 << numVars;
			ttype = st.nextToken();
			while (ttype == StreamTokenizer.TT_NUMBER) {
				int term = (int)st.nval;
				if (!min)
					term = (~term) & mask;
				Integer termInt = new Integer(term);
				if (terms.contains(termInt)) {
					System.err.println("Function spec contains redundant term "+term);
					return "Function spec contains redundant term "+term;
				}
				if ((term >= maxTerm) || (term < 0)) {
					System.err.println("Term "+term+ " is out of range (max="+(maxTerm-1)+")");
					return "Term "+term+ " is out of range (max="+(maxTerm-1)+")";
				}
				terms.add(termInt);
				ttype = st.nextToken();
			}

			if (ttype == StreamTokenizer.TT_WORD) {
				if (!((st.sval.equals("d")) || (st.sval.equals("D")))) {
					System.err.println("Function spec must have \"d\" or \"D\" for don't care terms, not "+st.sval);
					return "Internal error";
				}
			}
			else
				return "Success";

			ttype = st.nextToken();
			while (ttype == StreamTokenizer.TT_NUMBER) {
				int term = (int)st.nval;
				if (!min)
					term = (~term) & mask;
				Integer termInt = new Integer(term);
				if (terms.contains(termInt)) {
					System.err.println("Function spec contains redundant term "+term);
					return "Function spec contains redundant term "+term;
				}
				if ((term >= maxTerm) || (term < 0)) {
					System.err.println("Term "+term+ " is out of range (max="+maxTerm+")");
					return "Term "+term+ " is out of range (max="+maxTerm+")";
				}
				dontCares.add(termInt);
				ttype = st.nextToken();
			}
		}
		catch (java.io.IOException e) {
			System.err.println("Failed to read the input for some reason");
			return "Failed to read the input for some reason";
		}
		return "Success";
	}
}