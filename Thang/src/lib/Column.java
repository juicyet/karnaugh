package lib;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* The Column class implements one column of an implicant table. */
public class Column
{
  private List<Group> column;
  /* A reference to the table that this column is a part of */
  private ImplicationTable table;

  public List<Group> groups()
  {
    return column;
  }

  /*
   * Build the first column
   */
  public Column(List<Integer> terms, ImplicationTable t)
  {
    table = t;

    /* Allocate a list of empty groups that is numVars + 1 long. */
    column = new LinkedList<Group>();
    for (int v = 0; v <= table.numVars; v++) {
      column.add(new Group());
    }

    /* Add each term to the appropriate group, according to how many 1s it has. */
    
    for (Integer term : terms) {
      Implicant imp = new Implicant(term.intValue(), table.numVars);
      Group g = column.get(imp.numOnes());
      g.add(imp);
    }
  }

  /*
   * Build the next column from the previous column
   */
  public Column(Column prevCol)
  {
    table = prevCol.table;
    column = new LinkedList<Group>();

    /* Iterate through the groups, combining each pair of adjacent groups. */
    Iterator<Group> groups = prevCol.column.iterator();
    Group group0, group1 = groups.next();
    while (groups.hasNext()) {
      group0 = group1;
      group1 = groups.next();
      Group newGroup = new Group(group0, group1);
      column.add(newGroup);
    }
  }
}
