package lib;
public interface Bit
{
  public boolean equals(Bit other);
  public boolean oneZero(Bit other);
  public boolean bothConflict(Bit other);
  public boolean subset(Bit other);
  public boolean covers(int i);
  public Bit merge(Bit other);
}
