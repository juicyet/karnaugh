package lib;
public class BitZero implements Bit
{
  public boolean equals(Bit other)
  { return other instanceof BitZero; }

  public boolean oneZero(Bit other)
  { return other instanceof BitOne; }

  public boolean bothConflict(Bit other)
  { return other instanceof BitBoth; }

  public boolean subset(Bit other)
  { return (other instanceof BitBoth) || (other instanceof BitZero); }

  public boolean covers(int i)
  { return i == 0; }

  public Bit merge(Bit other)
  {
    if (other instanceof BitZero)
      return new BitZero();
    else
      return new BitBoth();
  }
  public String toString() { return "0"; }
}
