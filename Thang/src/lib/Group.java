package lib;
import java.util.Set;
import java.util.HashSet;

/* A group is just a collection of implicants that all have the same number of
 * 1s. */
public class Group
{
  private Set<Implicant> group;

  /* Simple "getter" method */
  public Set<Implicant> implicants()
  {
    return group;
  }

  /* Construct an empty group */
  public Group()
  {
    group = new HashSet<Implicant>();
  }

  /*
   * Build a group by merging two adjacent groups
   */
  public Group(Group g0, Group g1)
  {
    group = new HashSet<Implicant>();

    /* For each possible pair of implicants (imp0, imp1) drawn from group g0 and
     * group g1, add the merge of imp0 and imp1 if they pass the Quine-McCluskey
     * "off by one" test. */
    for (Implicant imp0 : g0.group) {
      for (Implicant imp1 : g1.group) {
//        System.out.println("" + imp0 + " )( " + imp1 + " ? "+imp0.offByOne(imp1));
        if (imp0.offByOne(imp1)) {
          group.add(new Implicant(imp0, imp1));
        }
      }
    }
  }

  /* Add an implicant to this group. */
  public void add(Implicant imp)
  {
    group.add(imp);
  }
}