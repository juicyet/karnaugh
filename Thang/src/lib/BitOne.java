package lib;
public class BitOne implements Bit
{
  public boolean equals(Bit other)
  { return other instanceof BitOne; }

  public boolean oneZero(Bit other)
  { return other instanceof BitZero; }

  public boolean bothConflict(Bit other)
  { return other instanceof BitBoth; }

  public boolean subset(Bit other)
  { return (other instanceof BitBoth) || (other instanceof BitOne); }

  public boolean covers(int i)
  { return i == 1; }

  public Bit merge(Bit other)
  {
    if (other instanceof BitOne)
      return new BitOne();
    else
      return new BitBoth();
  }
  public String toString() { return "1"; }
}
