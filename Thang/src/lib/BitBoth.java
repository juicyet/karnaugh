package lib;
public class BitBoth implements Bit
{
  public boolean equals(Bit other)
  { return other instanceof BitBoth; }

  public boolean oneZero(Bit other)
  { return false; }

  public boolean bothConflict(Bit other)
  { return (other instanceof BitZero) || (other instanceof BitOne); }

  public boolean subset(Bit other)
  { return other instanceof BitBoth; }

  public boolean covers(int i)
  { return true; }

  public Bit merge(Bit other)
  {
    return new BitBoth();
  }
  public String toString() { return "-"; }
}
